var current,
    size

// hide gallery down arrow after scrolling
$(".content").scroll(function(){
  offset = $(this).scrollTop();
  if (offset > 100) {
    $(".down").fadeOut();
  }
  else
  {
    $(".down").fadeIn();
  }
});

// show gallery link when scrolling over gallery images (mobile only)
function galleryScroll() {
  $('#slideshow').scroll(function() {

    var top = document.getElementById('gallery-wrapper').getBoundingClientRect().top;
    var bottom = document.getElementById('gallery-wrapper').getBoundingClientRect().bottom;

    if ( top < 70 && bottom > ($(window).height()) ) {
      $('#gallery-link').fadeIn('fast');
    } else {
      $('#gallery-link').fadeOut('fast');
    }

  });
}

/*======================== 
Gallery Down Arrow
========================*/

const downArrow = $('.down');

const firstGridItem = $('.first_grid_item');

downArrow.click(function() {

  const firstGridItemPosition = firstGridItem.offset().top;

  $('.content').animate({
      scrollTop: firstGridItemPosition
  }, 1000);

});

/*======================== 
MODAL TRIGGER
========================*/

const modalTrigger = $('.modal_trigger');

const modalName = modalTrigger.attr('data-modal');

const modal = $('.' + modalName + '_modal');

const modalClose = $('.modal_close');

const modalTransparentLayer = $('.modal_transparent_layer');

const modalVimeoIframe =  $('.' + modalName + '_modal iframe');

const vimeoPlayer = new Vimeo.Player(modalVimeoIframe);

modalTrigger.click(function(e){
  e.preventDefault();
  modal.addClass('open');
  vimeoPlayer.play();
});

modalClose.click(function(){
  $(this).closest('.modal').removeClass('open');
  vimeoPlayer.pause();
});

modalTransparentLayer.click(function(){
  $(this).closest('.modal').removeClass('open');
  vimeoPlayer.pause();
});


// slideshow functionality
function slideshow() {

  var slideNum = $('#first').index(this) + 1,
      size = $('#slideshow > li').length

  $('#slideshow > li').hide()
  $('#slideshow > li:eq(' + slideNum + ')').show()

  current = slideNum

  $('body').on('keydown', function(e){

    var dest

    if ((e.keyCode || e.which ) == 37) {

      dest = current - 1

      if (dest < 0) {

        dest = size - 1

      }

      $('#slideshow > li:eq(' + current + ')').hide()
      $('#slideshow > li:eq(' + dest + ')').show()

      current = dest

    } else if ((e.keyCode || e.which ) == 39) {

      dest = current + 1

      if (dest > size - 1) {

        dest = 0

      }

      $('#slideshow > li:eq(' + current + ')').hide()
      $('#slideshow > li:eq(' + dest + ')').show()

      current = dest

    }



  })

  $('body').on('click', function(e){

      var target = $(e.target)

      if ( target.context.className == 'next' ) {

        e.preventDefault();
        
        console.log(target.context.className);

        dest = current + 1

        if (dest > size - 1) {

          dest = 0

        }

        $('#slideshow > li:eq(' + current + ')').hide()
        $('#slideshow > li:eq(' + dest + ')').show()

        current = dest

      } else if ( target.context.className == 'prev' ) {

        e.preventDefault();

        console.log(target.context.className);

        dest = current - 1

        if (dest < 0) {

          dest = size

        }

        $('#slideshow > li:eq(' + current + ')').hide()
        $('#slideshow > li:eq(' + dest + ')').show()

        current = dest

      }
  })
}


// Left-side nav menu

function navigation() {

  $('.nav_toggle').on('click', function(){

    $('#nav').show()

  })

  $('.close_nav').on('click', function(){

    $('#nav').hide()

  })

  $('.nav_button').on('click', function(){
    dataTarget = $(this).attr('data-target');

    if ( !$(this).hasClass('close_nav') ) {

        $('.slide').each(function(){

          if ( $(this).attr('id') == dataTarget ) {

              $(this).show()

              current = $("#slideshow .slide").index(this);
              console.log($("#slideshow .slide").index(this));

            } else {

              $(this).hide()

              $('#nav').hide()

            }

        })

    }

  })

}


//Gallery Lightbox

function lightboxInit() {

    $('.lightbox_trigger').each(function() {

      $(this).on('click', function(e) {

        var imgLink = $(this).attr('href')

        e.preventDefault()

        $('body').append('<div class="lightbox"><img src="' + imgLink + '"><button class="close">close</button></div>')

        $('.close').on('click', function(){
          $(this).parent().hide()
        })

        $('body').on('click', '.lightbox', function(e) {
        e.stopPropagation()
        $('.lightbox').hide()

    })

    $('body').on('click', '.share', function(e) {
      e.stopPropagation()
    })

    $('body').on('keydown', function(e){

        if ((e.keyCode || e.which) == 27) {
            $('.lightbox').hide()
        }

      })

      })

    })

  }



$(function(){

    window.mobilecheck = function() {
      var check = false;
      (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
        return check; }

    if(!window.mobilecheck()){
      slideshow()
      navigation()
      lightboxInit()

    } else {
      galleryScroll()
      $('.lightbox_trigger').each(function(){
        $(this).on('click', function(e){
          e.preventDefault()
        })
      })
      console.log('we\'re mobile browsing')
    }

    // $('.prev_link').click(function(){
    //   $(this).addClass('blinker')
    // })

})

//make SVGs editable

$(function(){
    jQuery('img.svg').each(function(){
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        jQuery.get(imgURL, function(data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');

            // Add replaced image's ID to the new SVG
            if(typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if(typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass+' replaced-svg');
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');

            // Check if the viewport is set, else we gonna set it if we can.
            if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
            }

            // Replace image with new SVG
            $img.replaceWith($svg);

        }, 'xml');

    });
});
